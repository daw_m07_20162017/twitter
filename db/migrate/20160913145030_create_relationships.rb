class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.integer :follower_id #t.references :user, index: true, foreign_key: true
      t.integer :followed_id # No posem el t.references perquè volem canviar-li el nom, de user_id a followed_id

      t.timestamps null: false
    end
    add_index :relationships, :follower_id
    add_index :relationships, :followed_id
    add_index :relationships, [:follower_id, :followed_id], unique: true
  end
end
