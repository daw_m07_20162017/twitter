class AddUsernameToUsers < ActiveRecord::Migration

  # Tot el que s'escrigui dins d'aquest mètode, es traduirà a codi SQL i modificarà la BBDD
  def change
    # Afegeix una nova columna a la taula users, amb nom username i de tipus string
    add_column :users, :username, :string

    # Primer indexem username per poder buscar millor (ràpid). Segon ens assegurem
    #que el username és SEMPRE únic
    add_index :users, :username, unique: true
  end
end
