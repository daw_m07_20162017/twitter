class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Cridar a configure_permitted_parameters només quan s'usi la gemma devise
  before_action :configure_permitted_parameters, if: :devise_controller?

  # Protegir la BBDD i al mateix temps permetre l'actualització d'aquests camps
  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username,:email,:password,:password_confirmation,:remember_me])
    devise_parameter_sanitizer.permit(:log_in, keys: [:username,:email,:password,:password_confirmation,:remember_me])
    devise_parameter_sanitizer.permit(:account_update, keys: [:username,:email,:password,:password_confirmation,:remember_me])
#    devise_parameter_sanitizer.for(:sign_up){|u| u.permit(:username,:email,:password,:password_confirmation,:remember_me)}
#    devise_parameter_sanitizer.for(:log_in){|u| u.permit(:username,:email,:password,:password_confirmation,:remember_me)}
#    devise_parameter_sanitizer.for(:account_update){|u| u.permit(:username,:email,:password,:password_confirmation,:remember_me)}
  end

end
