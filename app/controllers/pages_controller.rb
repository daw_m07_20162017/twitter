# El controlador Pages conté el codi de totes les pàgines dins de /pages
class PagesController < ApplicationController

  # Codi back-end per a pages/index
  def index
  end

  # Codi back-end per a pages/home
  def home
    # Guardar els posts en una variable global
    @posts = Post.all

    #Crear la variable per al formulari
    @newPost = Post.new
  end

  # Codi back-end per a pages/profile
  def profile
    # Agafar l'username de la URL
    if User.find_by_username(params[:id])
      @username = params[:id]
    else
      # Redirigeix a error 404 (de moment a root)
      redirect_to root_path, notice: "User not found!"
    end

    # Guardar els posts en una variable global
    @posts = Post.all.where("user_id = ?", User.find_by_username(params[:id]).id)

    #Crear la variable per al formulari
    @newPost = Post.new

    # Crear variable amb els 5 ultims usuaris que han fet log in
    @toFollow = User.all.last(5)
  end

  # Codi back-end per a pages/explore
  def explore
    # Guardar els posts en una variable global
    @posts = Post.all
    #Crear la variable per al formulari
    @newPost = Post.new
    # Crear variable amb els 5 ultims usuaris que han fet log in
    @toFollow = User.all.last(5)
  end
end
