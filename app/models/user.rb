class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :posts, dependent: :destroy # Elimina tots els posts si l'usuari es eliminat
  #posem de forma diferent el has_many perquè tenim 2 relacions amb la mateixa taula o model
  has_many :active_relationships, class_name: "Relationship", foreign_key: "follower_id", dependent: :destroy
  has_many :passive_relationships, class_name: "Relationship", foreign_key: "followed_id", dependent: :destroy

  # has_many users"nom de la relacio, que farem servir en un futur per fer user.followers",
  #through: "taula o relació per la qual es relaciona aquesta variable",
  #source: "nom de la variable utilitzada en el model amb que es relaciona (Relationship)"
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower

  # helper methods

  # follow another user
  def follow(other, current)
   active_relationships.create(followed_id: other.id, follower_id: current.id)
  end

  # unfollow a user
  def unfollow(other, current)
   active_relationships.find_by(followed_id: other.id).destroy
    #active_relationships.find_by(followed_id: other.id, follower_id: current.id).destroy
  end

  # is following a user?
  def following?(other)
   following.include?(other)
  end

end
