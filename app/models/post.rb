class Post < ActiveRecord::Base
  belongs_to :user

  validates :user_id, presence: true #el user_id ve automaticament de fer la relacio
  validates :content, presence: true, length: { maximum: 140 } # Màxim 140 caràcters
  default_scope -> { order(created_at: :desc) } # Nous tweets primer
end
